function Class(base)
    local my_class = {}
    my_class.__index = my_class

    local my_class_mt = {}

    if type(base) == 'table' then
        for i,v in pairs(base) do
            my_class_mt[i] = v
        end
        my_class_mt.__index = my_class_mt
        setmetatable(my_class_mt, {__call = my_class_mt.new})
        my_class.super = my_class_mt
        my_class._base_type = base
    end

    my_class_mt.__call = function(class_tbl, ...)
            local new_instance = {}
            setmetatable(new_instance,class_tbl)
            new_instance:new(...)  
            return new_instance
        end

    my_class.is_a = function(self, klass)
        local m = getmetatable(self)
        while m do 
            if m == klass then return true end
            m = m._base_type
        end
        return false
    end

    setmetatable(my_class, my_class_mt)
    return my_class
end




-- -- class.lua
-- -- Compatible with Lua 5.1 (not 5.0).
-- function Class(base, init)
-- 	local c = {}    -- a new class instance
-- 	if not init and type(base) == 'function' then
-- 		init = base
-- 		base = nil
-- 	elseif type(base) == 'table' then
-- 	-- our new class is a shallow copy of the base class!
-- 		for i,v in pairs(base) do
-- 			c[i] = v
-- 		end
-- 		c._base = base
-- 	end
-- 	-- the class will be the metatable for all its objects,
-- 	-- and they will look up their methods in it.
-- 	c.__index = c

-- 	-- expose a constructor which can be called by <classname>(<args>)
-- 	local mt = {}
-- 	mt.__call = function(class_tbl, ...)
-- 		local obj = {}
-- 		setmetatable(obj,c)
-- 		if init then
-- 			init(obj,...)
-- 		else 
-- 			-- make sure that any stuff from the base class is initialized!
-- 			if base and base.init then
-- 				base.init(obj, ...)
-- 			end
-- 		end
-- 		return obj
-- 	end

-- 	c.init = init
-- 	c.is_a = function(self, klass)
-- 		local m = getmetatable(self)
-- 		while m do 
-- 			if m == klass then return true end
-- 			m = m._base
-- 		end
-- 		return false
-- 	end
-- 	setmetatable(c, mt)
-- 	return c
-- end


