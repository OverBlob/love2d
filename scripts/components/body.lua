local Body = Class()


function Body:new(inst, x, y, w, h)
    self.inst = inst
	self.x = x or 50
	self.y = y or 50
	self.w = w or 32
	self.h = h or 32
	self.dx = 1
	self.dy = 0
end

function Body:setPos(x, y)
	self.x = x
	self.y = y
end

function Body:setSize(w, h)
	self.w = w
	self.h = h
end

function Body:setDirection(dx, dy)
	self.dx = dx
	self.dy = dy
end

return Body