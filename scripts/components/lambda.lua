local Lambda = Class()

function Lambda:new(inst, fn)
	assert(type(fn) == 'function')
	self.inst = inst
	self.fn = fn
end

return Lambda