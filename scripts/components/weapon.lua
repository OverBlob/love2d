
local Weapon = Class()

function Weapon:new(inst)
    self.inst = inst
    self.damage = 10
    self.onattack = nil
end

function Weapon:SetOnAttack(fn)
    self.onattack = fn
end

function Weapon:OnAttack(attacker, target, projectile)
    if self.onattack then
        self.onattack(self.inst, attacker, target)
    end
    
    if self.inst.components.finiteuses then
	    self.inst.components.finiteuses:Use(self.attackwear or 1)
    end
end

function Weapon:LaunchProjectile(attacker, target)
	if self.projectile then

        if self.onprojectilelaunch then
            self.onprojectilelaunch(self.inst, attacker, target)
        end

	    local proj = SpawnPrefab(self.projectile)
	    if proj and proj.components.projectile then
	        proj.Transform:SetPosition(attacker.Transform:GetWorldPosition() )
	        proj.components.projectile:Throw(self.inst, target, attacker)
	    end
	end
end

return Weapon
