local Components = {}


Entity = Class()

function Entity:new()
	self.components = {}
	self.tags = {}
	self.event_listeners = nil
    self.event_listening = nil
	self.remove = false
	self.loaded = false
end

function Entity:destroy()
	self.remove = true
end

-- function Entity:addComponent(component)
-- 	assert(component.id)
-- 	self.components[component.id] = component
-- 	return self
-- end
local function loadComponent(name)
    
    if Components[name] == nil then
        Components[name] = require("components/"..name)
    end
    return Components[name]
end

function Entity:addComponent(name, ...)
    if self.components[name] then
        print("component "..name.." already exists!")
    end
    local cmp = loadComponent(name)
    assert(cmp, "component ".. name .. " does not exist!")
    
    local loadedcmp = cmp(self, ...)
    self.components[name] = loadedcmp
    return self
end

function Entity:getComponent(id)
	return self.components[id]
end

local function AddListener(t, event, inst, fn)
    local listeners = t[event]
    if not listeners then
        listeners = {}
        t[event] = listeners
    end
    
    local listener_fns = listeners[inst]
    if not listener_fns then
        listener_fns = {}
        listeners[inst] = listener_fns
    end
    
    table.insert(listener_fns, fn)
end

function Entity:listenForEvent(event, fn, source)
	assert(fn ~= nil, "Function param cant be nil")

	-- TODO: MOCHE!
	g_EventManager:hook("onMouseLeft", self)

    source = source or self
    
    if not source.event_listeners then
		source.event_listeners = {}
	end

    AddListener(source.event_listeners, event, self, fn)


    if not self.event_listening then
		self.event_listening = {}
    end
	
    AddListener(self.event_listening, event, source, fn)
    return self
end


function Entity:pushEvent(event, data)
	print("Entity:pushEvent " .. event)
	if self.event_listeners then
		local listeners = self.event_listeners[event]
		if listeners then
			for entity, fns in pairs(listeners) do
				for i,fn in ipairs(fns) do
					fn(self, data)
				end
			end
		end
	end
end