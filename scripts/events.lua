local EventManager = Class()


function EventManager:new()
	self.handlers = {}
end

function EventManager:add(evt_name)
	assert(self.handlers[evt_name] == nil, "Event type cannot be nil!")
	self.handlers[evt_name] = {}
	return self.handlers[evt_name]
end

function EventManager:remove(evt_name)
	self.handlers[evt_name] = nil
end

local function index_of(tbl, val)
	for i = 1, #tbl do
		if tbl[i] == val then return i end
	end
	return -1
end

-- function EventManager:hook(evt_name, callback)
-- 	assert(type(callback) == 'function', "The callback param must be a function!")
-- 	local evt_tbl = self.handlers[evt_name]

-- 	if evt_tbl == nil then
-- 		-- on crée l'event en passant
-- 		evt_tbl = self:add(evt_name)
-- 	end

-- 	-- déterminer si le callback n'existe pas déjà
-- 	if index_of(evt_tbl, callback) ~= -1 then
-- 		print("Callback is already registered for <" .. evt_name ..'>')
-- 	else
-- 		evt_tbl[#evt_tbl + 1] = callback
-- 	end
-- end
function EventManager:hook(evt_name, entity)
	assert(entity ~= nil, "The entity param cant be nil!")
	local evt_tbl = self.handlers[evt_name]

	if evt_tbl == nil then
		-- on crée l'event en passant
		evt_tbl = self:add(evt_name)
	end

	-- déterminer si le entity n'existe pas déjà
	if index_of(evt_tbl, entity) ~= -1 then
		print("entity is already registered for <" .. evt_name ..'>')
	else
		evt_tbl[#evt_tbl + 1] = entity
	end
end

function EventManager:clean(evt_name)
	if evt_name == nil then
		for k,v in pairs((self.handlers)) do
			self.handlers[evt_name] = {}
		end
	elseif self.handlers[evt_name] ~= nil then
		self.handlers[evt_name] = {}
	end
end

-- TODO modifier cette fonction pour prendre une etntiy à la place du callabk
function EventManager:unhook(evt_name, callback)
	assert(type(callback) == 'function', "The callback must be a function!")
	local evt_tbl = self.handlers[evt_name]
	assert(evt_tbl ~= nil, "Event <" .. evt_name .. "> doesn't exist!!")

	-- déterminer si le callback existe bien
	local index = index_of(evt_tbl, callback)
	if index ~= -1 then
		table.remove(evt_tbl, index)
	else
		print("Callback <" .. callback .. "> is already registered for <" .. evt_name ..">")
	end

end

function EventManager:push(evt_name, ...)
	assert(self.handlers[evt_name] ~= nil, "Event <" .. evt_name .. "> doesn't exist!")
	print("Event.invoke " .. evt_name)
	local evt_tbl = self.handlers[evt_name]
	for i = 1, #evt_tbl do
		evt_tbl[i]:pushEvent(evt_name, {...})
	end
end

return EventManager


