io.stdout:setvbuf('no')
local World = require "world"
local EventManager = require "events"

-- importer tous les components/systems
require "class"
require "system"

MOUSE_LEFT = 1
MOUSE_RIGHT = 2

g_World = nil
g_EventManager = nil


local function onattack(inst, data)
	local x, y = unpack(data)

	local body = inst:getComponent("body")
	local dx = (x - body.x)
	local dy = (y - body.y)

	local norm = math.sqrt(dx*dx + dy*dy)

	local ent = g_World:addEntity()
		:addComponent("body")
		:addComponent("lambda",(function(self, dt)
			local body = self:getComponent("body")
			body.x = body.x + 300 * body.dx * dt
			body.y = body.y + 300 * body.dy * dt
			end)
		)
	ent.components.body:setSize(5,5)
	ent.components.body:setPos(body.x,body.y)
	ent.components.body:setDirection(dx/norm,dy/norm)
end

function love.load()
	g_EventManager = EventManager()

	g_EventManager:add("onMouseLeft")
	g_EventManager:add("onMouseRight")


	g_World = World()
	g_World:addSystem(Renderer())
	g_World:addSystem(Functioner())

	g_World:addEntity()
		:addComponent("body", 250, 250)
		:addComponent("weapon")
		:listenForEvent("onMouseLeft", onattack)
	


	g_World:addEntity()
		:addComponent("body")
		:addComponent("lambda",(function(self, dt)
			local body = self:getComponent("body")
			body.x = body.x + 100 * dt
			end)
		)

end


function love.update(dt)
	g_World:update(dt)
end

function love.draw()
	g_World:draw()
end

function love.mousepressed( x, y, button, istouch )
	if button == MOUSE_LEFT then
		g_EventManager:push("onMouseLeft", x, y)
	end
	if button == MOUSE_RIGHT then
		g_EventManager:push("onMouseRight", x, y)
	end
end


function love.keypressed(k)
	if k == 'escape' then
		love.event.quit( ) -- Quit the game.
	end	
end