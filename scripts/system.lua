local System = Class()


function System:new(requires)
	assert(type(requires) == 'table')
	self.requires = requires

end

function System:match(entity)
	for i=1, #self.requires do
		if entity:getComponent(self.requires[i]) == nil then
			return false
		else
			return true
		end
	end
end	

function System:load(entity) 		end
function System:update(dt, entity) 	end
function System:draw(entity) 		end
function System:ui_draw(entity) 	end
function System:destroy(entity) 	end

------------------------------------------------------------------

Renderer = Class(System)


function Renderer:new()
	self.super({"body"})
end

function Renderer:load(entity)
	print("found one")
end

function Renderer:draw(entity)
	local body = entity:getComponent("body")

	love.graphics.rectangle("fill", body.x, body.y, body.w, body.h)
	love.graphics.print('Entity: ', 0, 0)
	love.graphics.print(#g_World.entities, 40, 0)
end

Functioner = Class(System)

function Functioner:new()
	self.super({"lambda"})
end

function Functioner:update(dt, entity)
	local fn = entity:getComponent("lambda").fn
	fn(entity, dt)
end


return Renderer


