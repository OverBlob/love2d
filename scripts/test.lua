function class(base)
    local my_class = {}
    my_class.__index = my_class

    local my_class_mt = {}

    if type(base) == 'table' then
        for i,v in pairs(base) do
            my_class_mt[i] = v
        end
        my_class_mt.__index = my_class_mt
        setmetatable(my_class_mt, {__call = my_class_mt.new})
        my_class.super = my_class_mt
        my_class._base_type = base
    end

    my_class_mt.__call = function(class_tbl, ...)
            local new_instance = {}
            setmetatable(new_instance,class_tbl)
            new_instance:new()  
            return new_instance
        end

    my_class.is_a = function(self, klass)
        local m = getmetatable(self)
        while m do 
            if m == klass then return true end
            m = m._base_type
        end
        return false
    end

    setmetatable(my_class, my_class_mt)
    return my_class
end

local Animal = class()

function Animal:new(a, b)
    print("je passe dans new Animal")
    self.a = a or 1
    self.b = b or 2
end

function Animal:hello()
    print("Hello Animal")
end

function Animal:mother(mot)
    print("Mother "..mot)
end

local Cat = class(Animal)

function Cat:new(couleur)
    print("je passe dans new Cat")
    self.couleur = couleur  or "vert"
    self.super()
end

-- function Cat:hello(who)
--     print("Hello Cat")
-- end

local animal = Animal(10,"prout")
animal:hello("you")
print(animal.b)

local cat1 = Cat("jaune")
local cat5 = Cat("rose")
cat1:hello("YAY")

print(cat1:is_a(Animal))


