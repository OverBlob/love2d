require("class")
require("entity")

local World = Class()

function World:new()
	self.entities = {}
	self.systems = {}
end

function World:addSystem(system)
	table.insert(self.systems, system)
end

function World:addEntity()
	local entity = Entity()
	table.insert(self.entities, entity)
	return entity
end

function World:getEntitiesWithComponents(components)
	local matched = {}

	for i, ent in ipairs(self.entities) do
		local matches = true
		for j, comp in ipairs(components) do
			if ent:getComponent(comp) == nil then matches = false end
		end

		if matches then table.insert(matched, ent) end
	end

	return matched
end

function World:update(dt)
	for i = #self.entities, 1, -1 do
		local entity = self.entities[i]
		if entity.remove then
			for i, system in ipairs(self.systems) do
				if system:match(entity) then
					system:destroy(entity)
				end
			end
			table.remove(self.entities, i)
		else
			for i, system in ipairs(self.systems) do
				if system:match(entity) then
					if entity.loaded == false then
						system:load(entity)
						entity.loaded = true
					end

					system:update(dt, entity)
				end
			end
		end
	end
end

function World:draw()
	for i=1, #self.entities do
		local entity = self.entities[i]
		for i, system in ipairs(self.systems) do
			if system:match(entity) then
				system:draw(entity)
			end
		end
	end
end

return World